from scipy.stats import norm
from scipy.optimize import fmin
from scipy.misc import derivative
from scipy.integrate import quad

# Cp letigation cost plaintif
# Cd letigation cost defentant
# W Judgment size: 
# p liklihood of plaintif going to trial
# f(.) p is distributed with density function 
# F(.) p is distributed with distribution funtion
# a Liability lower bound: 
# b Liability upper bound: 
# -Cp + a * W > 0
# S setlement amount
# defentant trial cost Cd + p * W
# Defentant accept setlement if S <= Cd + p * W
# or p >= (S - Cd) / W
# defendant accept offer p >= q(s)
# q(S) = (S - Cd) / W
# q(S) borderline value

Cp = 20000 
Cd = 20000
W = 10000000
a = 0.1
b = 0.2

p = (a + b) / 2
p = a
#print("liklihood of plaintif going to trial")
#print(p)

#S = Cd + p * W

#print("setlement amount")
#while (S >= Cd + b * W):
#    S = S - 1

#S = S / b
#print("maximum expected value :")
#print(S) 
#print("borderline value")
#print((S - Cd ) / W)

#print("liklihood of accepting")
#print(1 - norm.cdf((S - Cd ) / W))

def q(S):
    return ((S - Cd ) / W)

def F(x):
    return norm.cdf(x)

def f(x):
    return norm.pdf(x)

#def A(S):
#    a = derivative(Ader, 1.0, 1.0, 1, args=(S))
#    return a

#def Ader(S):
#    return ( 1 - F(q(S))) - (((Cp+Cd)/W) * f(q(S)))

#def ff(x):
    #return (x * f(x))
#    return (f(x))

def helpff(S):
    top = q(S)
    x2 = lambda x: x* f(x)
    res, err = quad(x2, a, top)
    return res

def A(S):
    #return ((1-F(q(S))) * S + F( q( S ) ) * (- Cp + W * (helpff(S)/F(q(S)))))
    return ((1-F(q(S))) * S + F( q( S ) ) * (- Cp + W * (((((q(S)**2-a**2)*f(q(S)))/2))/F(q(S)))))


def propPlainW(S):
    return (((((q(S)**2-a**2)*f(q(S)))/2))/F(q(S)))

max_S = fmin(lambda S: -A(S), 0)

#while (S >= Cd + b * W):
#    max_S = max_S + A(S)
#print("max")
#print(max_S)

#print("plaintifs liklihood of wining")
#print(helpff(1000000)/F(q(1000000)))

#rast = 1-F(q(max_S))
#print("liklihood of setl:")
#print(rast)


print("Maximum expected value:")
print(Cd + propPlainW(max_S) * W) 
print("Optimal settlement demand:") 
print(max_S/10) 
print("Likelihood of settlement:")
print((1-F(q(max_S)))/10)  
print("Probability of Plaintiff  winning:")
print(propPlainW(max_S))
print("Borderline type:")
print(q(max_S)/10)

