#!/usr/bin/env python3
"""Riskter API"""

from time import tzset, strftime
from subprocess import call
from json import load
import shelve
from os import environ
from config import Config

from scipy.stats import norm
from scipy.optimize import fmin
from scipy.misc import derivative
from scipy.integrate import quad

from numpy import ndarray

def set_timezone(timezone):
    """List of timezones at /usr/share/zoneinfo/"""
    environ['TZ'] = timezone
    tzset()

class Riskter:
    """TODO"""
    def __init__(self):

        set_timezone(Config.timezone)
        
    def caseA(self, Cp, Cd, W, a, b):

        # Cp letigation cost plaintif
        # Cd letigation cost defentant
        # W Judgment size: 
        # p liklihood of plaintif going to trial
        # f(.) p is distributed with density function 
        # F(.) p is distributed with distribution funtion
        # a Liability lower bound: 
        # b Liability upper bound: 
        # -Cp + a * W > 0
        # S setlement amount
        # defentant trial cost Cd + p * W
        # Defentant accept setlement if S <= Cd + p * W
        # or p >= (S - Cd) / W
        # defendant accept offer p >= q(s)
        # q(S) = (S - Cd) / W
        # q(S) borderline value

        #Cp = 20000 
        #Cd = 20000
        #W = 10000000
        #a = 0.1
        #b = 0.2

        print(Cp)

        def q(S):
            val = ((S - Cd ) / W)
            return val


        def F(x):
            return norm.cdf(x)



        def f(x):
            return norm.pdf(x)



        def A(S):
            return ((1-F(q(S))) * S + F( q( S ) ) * (- Cp + W * (((((q(S)**2-a**2)*f(q(S)))/2))/F(q(S)))))


        def propPlainW(S):
            return (((((q(S)**2-a**2)*f(q(S)))/2))/F(q(S)))


        max_S = fmin(lambda S: -A(S), 0).astype(int)

        print("Maximum expected value:")
        print(Cd + propPlainW(max_S) * W) 
        print("Optimal settlement demand:") 
        print(max_S/10) 
        print("Likelihood of settlement:")
        print((1-F(q(max_S)))/10)  
        print("Probability of Plaintiff  winning:")
        print(propPlainW(max_S))
        print("Borderline type:")
        print(q(max_S)/10)
 

        #dat['MaximumExpectedValue'] = (Cd + propPlainW(max_S) * W) 
        #dat['OptimalSettlementDemand'] = (max_S/10)
        #dat['LikelihoodOfSettlement'] = ((1-F(q(max_S)))/10)  
        #dat['ProbabilityOfPlaintiffWinning'] = (propPlainW(max_S))
        #dat['BorderlineType'] = (q(max_S)/10)
        dat = {}
        dat['Maximum Expected Value'] = (Cd + propPlainW(max_S) * W).astype(int)[0]
        dat['Optimal Settlement Demand'] = (max_S/10).astype(int)[0]
        dat['Likelihood of Settlement'] = ((1-F(q(max_S)))/10)[0]
        dat['Probability of Plaintiff Winning'] = (propPlainW(max_S))[0]
        dat['Borderline Type'] = (q(max_S)/10)[0] 

        res = dat
        return res
