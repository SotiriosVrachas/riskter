class Config: 
    """List of timezones at /usr/share/zoneinfo/""" 
    timezone = "America/Los_Angeles" 
    timeformat = "%b %e, %Y %H:%M" 
