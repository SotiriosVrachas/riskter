#!/usr/bin/env python3
"""Riskter API"""

import asyncio
from flask import Flask, jsonify, send_from_directory, request 
from model import Riskter

API = Flask(__name__)
API.config['JSON_AS_ASCII'] = False
RISK = Riskter()


def call_in_background(target, *, loop=None, executor=None):
    """Schedules and starts target callable as a background task

    If not given, *loop* defaults to the current thread's event loop
    If not given, *executor* defaults to the loop's default executor

    Returns the scheduled task.
    """
    if loop is None:
        loop = asyncio.get_event_loop()
    if callable(target):
        return loop.run_in_executor(executor, target)


@API.route('/<path:path>')
def send_js(path):
    return send_from_directory('static', path)


@API.route('/searches', methods=['GET'])
def show_searches_status():
    """TODO"""
    return jsonify(RISK.status()), 200


@API.route('/searches/<int:search_id>', methods=['GET'])
def show_search(search_id):
    """json show search"""
    return jsonify(RISK.searches[search_id]), 200


@API.route('/searches/<int:search_id>', methods=['DELETE'])
def delete_search(search_id):
    """TODO"""
    RISK.delete(search_id, True)
    return jsonify(), 204


@API.route('/riskter/caseA', methods=['POST'])
def caseAsrv():
    """TODO"""
    content = request.get_json(silent=True)
    Cd = float(content[0]['value'])
    Cp = float(content[1]['value'])
    W = float(content[2]['value'])
    a = float(content[3]['value'])
    b = float(content[4]['value'])
    print(Cd) 
    res = RISK.caseA(Cd,Cp,W,a,b)
    print(content)

    return jsonify(str(res)), 200

@API.route('/searches/ghost', methods=['GET'])
def show_ghost():
    """TODO"""
    search_id = RISK.ghost()['id']
    booo = RISK.searches[search_id]
    booo['deldate'] = RISK.ghost()['deldate']
    booo['deltime'] = RISK.ghost()['deltime']
    booo['delmin'] = RISK.ghost()['delmin']
    booo['score'] = RISK.ghost()['score']
    return jsonify(booo), 200

def api():
    """TODO"""
    print('\n\n\n***   Try http://127.0.0.1:5000/index.html    ***\n\n\n')
    API.run(host='0.0.0.0', port=5000)


if __name__ == '__main__':
    api()

